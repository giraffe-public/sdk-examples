import * as THREE from "three";

const loader = new THREE.ObjectLoader();

export const parseScene = (data: string) => {
  const parsedJSON = JSON.parse(data);
  const parsed = loader.parse(parsedJSON) as THREE.Scene;

  parsed.children.forEach((child) => {
    child.traverse((c) => {
      if (c instanceof THREE.Mesh) {
        if (c.material["emissiveIntensity"]) {
          c.material["emissiveIntensity"] = 0;
        }
      }
    });
  });

  return parsed;
};

export const addClippingPlanes = (scene: THREE.Scene, plane: THREE.Plane) => {
  const sectionCuts: THREE.Mesh[] = [];
  const gutsMaterial = new THREE.MeshBasicMaterial({
    color: "black",
    side: THREE.BackSide,
    clippingPlanes: [plane],
    clipShadows: true,
  });

  scene.children.forEach((child) => {
    child.traverse((c) => {
      if (c instanceof THREE.Mesh) {
        if (c.material instanceof THREE.MeshPhongMaterial) {
          (c.material as THREE.MeshPhongMaterial).clippingPlanes = [plane];
          (c.material as THREE.MeshPhongMaterial).clipShadows = true;
          (c.material as THREE.MeshPhongMaterial).side = THREE.FrontSide;
          (c.material as THREE.MeshPhongMaterial).needsUpdate = true;

          const sectionCut = c.clone(true);
          sectionCut.material = gutsMaterial;
          sectionCut.castShadow = false;
          sectionCuts.push(sectionCut);
        }
      }
    });
  });

  scene.add(...sectionCuts);
};
