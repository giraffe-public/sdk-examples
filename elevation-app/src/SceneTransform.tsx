import * as THREE from "three";
import { Euler, Matrix4, Quaternion, Vector3 } from "three";

const THREE_MAP_SCALE = 2000 * 512;
export class SceneTransform {
  public readonly center: [number, number];

  constructor(public readonly centerLngLat: [number, number]) {
    this.center = lngLatToMercatorCoord(centerLngLat);
  }

  private projectXY([lng, lat]: [number, number]): [number, number] {
    const [x, y] = lngLatToMercatorCoord([lng, lat]);
    return [
      (x - this.center[0]) * THREE_MAP_SCALE,
      -(y - this.center[1]) * THREE_MAP_SCALE,
    ];
  }

  public pointToVec([lng, lat]: [number, number]): THREE.Vector3 {
    return new THREE.Vector3(...[...this.projectXY([lng, lat]), 0]);
  }
}

export function lngLatToMercatorCoord([lng, lat]: [number, number]): [
  number,
  number
] {
  return [mercatorXfromLng(lng), mercatorYfromLat(lat)];
}

function mercatorXfromLng(lng: number): number {
  return (180 + lng) / 360;
}

function mercatorYfromLat(lat: number): number {
  return (
    (180 -
      (180 / Math.PI) *
        Math.log(Math.tan(Math.PI / 4 + (lat * Math.PI) / 360))) /
    360
  );
}

export type Decomposition = {
  position: [number, number, number];
  rotation: [number, number, number];
  scale: [number, number, number];
};

export function decompose(matrix: Matrix4): Decomposition {
  const position = new Vector3();
  const quaternion = new Quaternion();
  const rotation = new Euler();
  const scale = new Vector3();

  matrix.decompose(position, quaternion, scale);
  rotation.setFromQuaternion(quaternion);

  return {
    position: position.toArray() as [number, number, number],
    rotation: rotation.toArray() as [number, number, number],
    scale: scale.toArray() as [number, number, number],
  };
}
