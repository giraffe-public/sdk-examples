import { rpc } from "@gi-nx/iframe-sdk";
import { Canvas } from "@react-three/fiber";
import { useEffect, useState } from "react";
import Select from "react-select";
import * as THREE from "three";
import "./App.css";
import { addClippingPlanes, parseScene } from "./parseScene";
import { SceneTransform } from "./SceneTransform";
import { useGiraffeState } from "./useGiraffeState";
import { World } from "./World";

export type PreStackedSection = {
  geometry: any;
  properties: {
    elevation?: boolean;
    id: string;
    ix: number;
  };
};

function App() {
  // const count = useGiraffeState("bakedSections");

  const { scene, elevations, sceneTranform, clippingPlane } = useScene();
  const [selectedElevationId, setSelectedElevationId] = useState<string>();
  const selectedElevation = elevations.find(
    e => e.properties.id === selectedElevationId
  );

  return (
    <>
      <div
        style={{
          display: "flex",
          flexDirection: "column",
          height: "100%",
          background: "rgba(0,0,0,0.5)",
        }}
      >
        <DrawSectionButton />
        <ElevationSelect
          elevations={elevations}
          selectedElevationId={selectedElevationId}
          setSelectedElevationId={setSelectedElevationId}
        />
        <Canvas
          shadows
          camera={{ position: [10, 12, 12], fov: 25 }}
          style={{
            height: "100%",
            background: "#def5ff",
          }}
          onPointerDown={e => {}}
        >
          {sceneTranform && selectedElevation && scene && (
            <World
              scene={scene}
              sceneTransform={sceneTranform}
              selectedElevation={selectedElevation}
              clippingPlane={clippingPlane}
            />
          )}
        </Canvas>
      </div>
    </>
  );
}

const DrawSectionButton = () => {
  const handleClick = async () => {
    const polygon = await rpc.invoke("getUserDrawnPolygon");
    if (!polygon) return;
    polygon.geometry.type = "LineString";
    polygon.geometry.coordinates = [
      polygon.geometry.coordinates[0][0],
      polygon.geometry.coordinates[0][1],
    ];
    polygon.properties["elevation"] = true;
    rpc.invoke("createRawSection", polygon);
  };

  return <button onClick={handleClick}>Click to draw a section</button>;
};

const ElevationSelect = ({
  elevations,
  selectedElevationId,
  setSelectedElevationId,
}: {
  elevations: PreStackedSection[];
  selectedElevationId: string | undefined;
  setSelectedElevationId: (e: string) => void;
}) => {
  const options = elevations.map(e => ({
    value: e.properties.id,
    label: e.properties.id,
  }));

  return (
    <Select
      options={options}
      value={
        selectedElevationId
          ? {
              value: selectedElevationId,
              label: selectedElevationId,
            }
          : null
      }
      onChange={e => {
        if (!e) return;
        setSelectedElevationId(e.value);
      }}
      placeholder="Select an elevation"
    />
  );
};

const clippingPlane = new THREE.Plane(new THREE.Vector3(0, 0, 1), 0);

const useScene = () => {
  const [scene, setScene] = useState<THREE.Scene>(); // State to hold GLB data
  const [sceneTranform, setSceneTransform] = useState<SceneTransform>();
  const rawSections = useGiraffeState("rawSections").features;
  const elevations = (Object.values(rawSections) as PreStackedSection[]).filter(
    (s: PreStackedSection) => s.properties.elevation !== undefined
  );

  useEffect(() => {
    rpc
      .invoke("getThreeScene")
      .then((data: string) => {
        const parsed = parseScene(data);
        addClippingPlanes(parsed, clippingPlane);
        setScene(parsed);
      })

      .then(() => {
        rpc.invoke("getSceneTransform").then((data: any) => {
          const parsed = JSON.parse(data);
          const sceneTranform = new SceneTransform(parsed.centerLngLat);
          setSceneTransform(sceneTranform);
        });
      });
  }, []);

  return { scene, elevations, sceneTranform, clippingPlane };
};

export default App;
