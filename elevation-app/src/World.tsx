import { OrthographicCamera } from "@react-three/drei";
import { useRef, useEffect, useState } from "react";
import * as THREE from "three";
import { PreStackedSection } from "./App";
import { SceneTransform, decompose } from "./SceneTransform";
import { useThree } from "@react-three/fiber";

const UP = new THREE.Vector3(0, 0, 1);

export const World = ({
  scene,
  sceneTransform,
  selectedElevation,
  clippingPlane,
}: {
  scene: THREE.Scene;
  sceneTransform: SceneTransform;
  selectedElevation: PreStackedSection;
  clippingPlane: THREE.Plane;
}) => {
  const { midPoint, tangent, perpendicular } = getClippingRay(
    sceneTransform,
    selectedElevation
  );

  const flipped = selectedElevation.properties.elevation === true;
  console.log("flipped", flipped, selectedElevation.properties.id);

  const cameraXform = new THREE.Matrix4().identity();
  const translation = new THREE.Matrix4().makeTranslation(midPoint);
  const rotation = new THREE.Matrix4().lookAt(
    midPoint,
    flipped
      ? midPoint.clone().add(perpendicular.clone().multiplyScalar(-1))
      : midPoint.clone().sub(perpendicular),
    UP
  );
  cameraXform.multiply(translation);
  cameraXform.multiply(rotation);

  clippingPlane.setFromNormalAndCoplanarPoint(perpendicular, midPoint);

  const xAxis = tangent.clone();
  const YAxis = new THREE.Vector3(0, 0, 1);

  return (
    <>
      <Camera xAxis={xAxis} yAxis={YAxis} {...decompose(cameraXform)} />
      {scene && <primitive object={scene} />}
    </>
  );
};

const getClippingRay = (
  sceneTransform: SceneTransform,
  selectedElevation: PreStackedSection
) => {
  const startPt = sceneTransform.pointToVec(
    selectedElevation.geometry.coordinates[
      selectedElevation.properties.elevation ? 0 : 1
    ]
  );
  const endPt = sceneTransform.pointToVec(
    selectedElevation.geometry.coordinates[
      selectedElevation.properties.elevation ? 1 : 0
    ]
  );

  const midPoint = startPt.clone().lerp(endPt, 0.5);
  const tangent = endPt.clone().sub(startPt).normalize();
  const perpendicular = new THREE.Vector3(0, 0, 1).cross(tangent);

  return { midPoint, tangent, perpendicular };
};

function Camera({
  position,
  rotation,
  xAxis,
  yAxis,
}: {
  position: [number, number, number];
  rotation: [number, number, number];
  xAxis: THREE.Vector3;
  yAxis: THREE.Vector3;
}) {
  const cameraRef = useRef<THREE.OrthographicCamera>(null);

  useEffect(() => {
    if (cameraRef.current) {
      const cam = cameraRef.current;
      cam.up.set(0, 0, 1);
      cam.position.set(...position);
      cam.rotation.set(...rotation);
      cam.updateProjectionMatrix();
    }
  }, [position, rotation]);

  usePan(xAxis, yAxis);
  useZoom();

  return (
    <OrthographicCamera
      ref={cameraRef}
      makeDefault
      zoom={100}
      near={0.001}
      far={100}
    />
  );
}

const useZoom = () => {
  const { camera, gl } = useThree();

  const onMouseWheel = (event: WheelEvent) => {
    camera.zoom -= event.deltaY * 0.1;
    if (camera.zoom < 1) camera.zoom = 1;
    camera.updateProjectionMatrix();
  };

  useEffect(() => {
    gl.domElement.addEventListener("wheel", onMouseWheel);
  }, [camera, gl]);
};

const usePan = (xAxis: THREE.Vector3, yAxis: THREE.Vector3) => {
  const { camera, gl } = useThree();
  const [isPanning, setIsPanning] = useState(false);
  const [startPos, setStartPos] = useState<{ x: number; y: number } | null>(
    null
  );

  useEffect(() => {
    if (camera instanceof THREE.PerspectiveCamera) return;

    const onPointerDown = (event: PointerEvent) => {
      setIsPanning(true);
      setStartPos({ x: event.clientX, y: event.clientY });
    };

    const onPointerMove = (event: PointerEvent) => {
      if (!isPanning || !startPos) return;

      const deltaX =
        ((event.clientX - startPos.x) * (camera.right - camera.left)) /
        gl.domElement.clientWidth;
      const deltaY =
        ((event.clientY - startPos.y) * (camera.top - camera.bottom)) /
        gl.domElement.clientHeight;

      // camera.position.x -= deltaX;
      // camera.position.y += deltaY;
      camera.position.add(xAxis.clone().multiplyScalar(deltaX * 0.01));
      camera.position.add(yAxis.clone().multiplyScalar(deltaY * 0.01));

      camera.updateProjectionMatrix();

      setStartPos({ x: event.clientX, y: event.clientY });
    };

    const onPointerUp = () => setIsPanning(false);

    gl.domElement.addEventListener("pointerdown", onPointerDown);
    gl.domElement.addEventListener("pointermove", onPointerMove);
    gl.domElement.addEventListener("pointerup", onPointerUp);
    gl.domElement.addEventListener("pointerleave", onPointerUp);

    return () => {
      gl.domElement.removeEventListener("pointerdown", onPointerDown);
      gl.domElement.removeEventListener("pointermove", onPointerMove);
      gl.domElement.removeEventListener("pointerup", onPointerUp);
      gl.domElement.removeEventListener("pointerleave", onPointerUp);
    };
  }, [camera, gl, isPanning, startPos, xAxis, yAxis]);
};
