import { giraffeState } from "@gi-nx/iframe-sdk";
import { useRef, useSyncExternalStore } from "react";

const isEqual = (a, b) => JSON.stringify(a) === JSON.stringify(b);

export function useGiraffeState(key) {
  const storeCache = useRef({});

  return useSyncExternalStore(
    (update) => {
      const listenerId = giraffeState.addListener([key], () => {
        update();
      });
      return () => {
        giraffeState.removeListener(listenerId);
      };
    },
    () => {
      // because some keys are wrapped by FeatureCollection, they need to be checked for actual changes
      if (
        storeCache.current[key] === undefined ||
        !isEqual(storeCache.current[key], giraffeState.get(key))
      ) {
        storeCache.current[key] = giraffeState.get(key);
        return storeCache.current[key];
      } else {
        return storeCache.current[key];
      }
    }
  );
}
