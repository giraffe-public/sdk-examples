# SDK Examples

Each directory contains an example web app using the Giraffe SDK.

* [Getting started example - reading and writing to the Giraffe map](getting-started)
* [Persisting custom data in a Giraffe project ](saving-state)
* [Example - Using Mapbox IsoChrone API](isochrone)
* [Example - Using ShapeDiver to run a Grasshopper module](shapediver)

