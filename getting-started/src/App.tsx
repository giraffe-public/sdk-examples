import { giraffeState, rpc } from "@gi-nx/iframe-sdk";
import { Feature, Polygon } from "geojson";
import { isEqual } from "lodash";
import { useEffect, useState } from "react";
import NewWindow from "react-new-window";

import "./App.css";
import { useSetRightBarWidth } from "./hooks";
import { useGiraffeState } from "./useGiraffeState";
import { createBoundaryPoly, fetchOverpass } from "./utils";

const TEMP_LAYER_NAME = "Overpass";

function App() {
  const [isPopupOpen, setIsPopupOpen] = useState(false);

  useSetRightBarWidth(600);
  // listen to changes in giraffe geometries (bakedSections)
  const bakedSections = useGiraffeState("bakedSections");
  const [searchBoundary, setSearchBoundary] = useState<Feature<Polygon>>();
  const [writingLayerData, setWritingLayerData] = useState(false);
  const [overpassFeatures, setOverpassFeatures] = useState<Feature[]>([]);

  // add temp layer and open lens
  useEffect(() => {
    rpc
      .invoke("addTempLayerGeoJSON", [
        TEMP_LAYER_NAME,
        { type: "FeatureCollection", features: [] },
        {},
        {}, //lens config here
      ])
      .then(() => {
        rpc.invoke("activateLensLayer", [TEMP_LAYER_NAME]);
      });
  }, []);

  // run overpass query when the boundary changes
  useEffect(() => {
    const firstOverpassPolygon = bakedSections.features.filter(
      f =>
        f.geometry.type === "Polygon" &&
        f.properties.overpassQueryBoundary === true
    )[0];
    setSearchBoundary(firstOverpassPolygon);
    if (firstOverpassPolygon) {
      if (!isEqual(firstOverpassPolygon, searchBoundary)) {
        setWritingLayerData(true);
        fetchOverpass(firstOverpassPolygon).then(overpassFeatures => {
          rpc
            .invoke("updateTempLayerGeoJSON", [
              TEMP_LAYER_NAME,
              {
                type: "FeatureCollection",
                features: overpassFeatures,
              },
            ])
            .then(e => {
              console.log(e);
              setOverpassFeatures(overpassFeatures);
              setWritingLayerData(false);
            });
        });
      }
    }
  }, [bakedSections]);

  const columns = overpassFeatures.reduce((acc, f) => {
    Object.keys(f.properties).forEach(k => {
      if (!acc.includes(k)) {
        acc.push(k);
      }
    });
    return acc;
  }, [] as string[]);

  return (
    <div className="App p-4 flex flex-col gap-2 text-base">
      {!searchBoundary && (
        <div className="p-3 bg-red-100 border-2 border-red-800 rounded-lg font-semibold flex flex-col gap-2 items-center">
          <div>You haven't set the search boundary yet</div>
          <button
            className="bg-white font-normal border text-gray-500 border-gray-500 px-2 hover:bg-gray-100"
            onClick={() =>
              rpc.invoke("createRawSection", [
                createBoundaryPoly(giraffeState.get("mapView").bounds),
              ])
            }
          >
            Create a boundary
          </button>
        </div>
      )}

      {writingLayerData && (
        <div className="flex space-x-2 justify-center items-center bg-white h-screen dark:invert">
          <span>Getting features from Overpass turbo</span>
          <div className="h-4 w-4 bg-black rounded-full animate-bounce [animation-delay:-0.3s]"></div>
          <div className="h-4 w-4 bg-black rounded-full animate-bounce [animation-delay:-0.15s]"></div>
          <div className="h-4 w-4 bg-black rounded-full animate-bounce"></div>
        </div>
      )}

      <div className="text-center p-2 bg-blue-100 rounded border-2 border-blue-800">
        <a
          target="_blank"
          rel="noreferrer"
          href="https://wiki.openstreetmap.org/wiki/Overpass_API"
        >
          More about the Overpass API
        </a>
      </div>

      <button
        className="bg-white font-normal border text-gray-500 border-gray-500 px-2 hover:bg-gray-100"
        disabled={overpassFeatures.length === 0}
        onClick={() => setIsPopupOpen(true)}
      >
        Open Results Table
      </button>

      <button
        className="bg-white font-normal border text-gray-500 border-gray-500 px-2 hover:bg-gray-100"
        disabled={overpassFeatures.length === 0}
        onClick={() =>
          rpc.invoke("updateLayerStyle", [
            TEMP_LAYER_NAME,
            {
              showLabels: true,
              displayKeys: ["name", "amenity"],
            },
          ])
        }
      >
        Add labels to map
      </button>

      <button
        className="bg-white font-normal border text-gray-500 border-gray-500 px-2 hover:bg-gray-100"
        disabled={overpassFeatures.length === 0}
        onClick={() =>
          rpc.invoke("enableBottomBarIframe", ["https://roland50.studio/", 500])
        }
      >
        Add iframe to bottom of map
      </button>

      {isPopupOpen && (
        <NewWindow
          // NB center="parent" is not supported when this is used in an iframe
          center="screen"
          onUnload={() => setIsPopupOpen(false)}
        >
          <table>
            <thead>
              <tr>
                {columns.map((c, i) => (
                  <th key={i}>{c}</th>
                ))}
              </tr>
            </thead>
            <tbody>
              {overpassFeatures.map((f, i) => (
                <tr key={i}>
                  {columns.map((c, j) => (
                    <td key={j}>{f.properties[c]}</td>
                  ))}
                </tr>
              ))}
            </tbody>
          </table>
        </NewWindow>
      )}
    </div>
  );
}

export default App;
