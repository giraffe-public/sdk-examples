import { rpc } from "@gi-nx/iframe-sdk";
import { get, set } from "lodash";
import { useEffect } from "react";
import { useGiraffeState } from "./useGiraffeState";

export const useSetRightBarWidth = (rightBarWidth: number) => {
  const selectedPa = useGiraffeState("selectedProjectApp");
  useEffect(() => {
    if (selectedPa) {
      const existingWidth = get(selectedPa, "public.layout.rightBarWidth", 386);
      if (existingWidth !== rightBarWidth) {
        const cloned = structuredClone(selectedPa);
        set(cloned, "public.layout.rightBarWidth", rightBarWidth);
        rpc.invoke("updateProjectApp", [selectedPa.app, cloned]);
      }
    }
  }, [selectedPa]);
};
