import { giraffeState, rpc } from "@gi-nx/iframe-sdk";
import { Feature, Polygon } from "geojson";
import { useEffect, useState } from "react";

import "./App.css";
import { fetchOverpass } from "./utils";

const TEMP_LAYER_NAME = "Overpass";

const ActionStep = ({ onClick, ix, children, disabled = false }) => {
  return (
    <div className="flex justify-between">
      <div className="rounded-full bg-gray-500 text-white w-8 h-8 flex items-center justify-center">
        {ix}
      </div>
      <button
        className="flex-grow ml-4 bg-white font-normal border text-blue-500 border-blue-500 px-2 hover:bg-gray-100 disabled:opacity-50 disabled:bg-gray-200"
        onClick={onClick}
        disabled={disabled}
      >
        {children}
      </button>
    </div>
  );
};

function App() {
  // listen to changes in giraffe geometries (bakedSections)
  const [searchBoundary, setSearchBoundary] = useState<Feature<Polygon>>();
  const [writingLayerData, setWritingLayerData] = useState(false);
  const [overpassFeatures, setOverpassFeatures] = useState<Feature[]>([]);

  // add temp layer and open lens
  useEffect(() => {
    rpc
      .invoke("addTempLayerGeoJSON", [
        TEMP_LAYER_NAME,
        { type: "FeatureCollection", features: [] },
        {},
        {}, //lens config here
      ])
      .then(() => {
        rpc.invoke("activateLensLayer", [TEMP_LAYER_NAME]);
      });
  }, []);

  // when the user clicks on the map
  useEffect(() => {
    giraffeState.addListener(["mapSelectCoord"], () => {
      // query features at location
      rpc
        .invoke("queryRenderedFeatures", [
          giraffeState.get("mapSelectCoord"),
          // { layers: [`GIRTMP-${TEMP_LAYER_NAME}.0`] },
        ])
        .then(features => {
          console.log({ features });
          if (features.length) {
            rpc.invoke("addIframePopup", [
              `https://seleniumbase.io/w3schools/demo_iframe.htm`,
              giraffeState.get("mapSelectCoord"),
              { closeButton: false, maxWidth: "none" },
              450,
              300,
              false,
            ]);
          }
          // rpc.invoke("addHtmlPopup", [
          //   "<h1>Hi!</h1><p>This is a popup</p>",
          //   giraffeState.get("mapSelectCoord"),
          //   {},
          // ]);
        });
    });
  }, []);

  // run overpass query when the boundary changes
  useEffect(() => {
    console.log({ searchBoundary });
    if (searchBoundary) {
      console.log("what");
      setWritingLayerData(true);
      fetchOverpass(searchBoundary).then(overpassFeatures => {
        console.log({ overpassFeatures });
        rpc
          .invoke("updateTempLayerGeoJSON", [
            TEMP_LAYER_NAME,
            {
              type: "FeatureCollection",
              features: overpassFeatures,
            },
          ])
          .then(e => {
            console.log(e);
            setOverpassFeatures(overpassFeatures);
            setWritingLayerData(false);
          });
      });
    }
  }, [searchBoundary]);

  return (
    <div className="App p-4 flex flex-col gap-2 text-base h-full  ">
      <ActionStep
        ix={1}
        onClick={() =>
          rpc.invoke("getLassoShape", []).then((shape: Feature<Polygon>) => {
            setSearchBoundary(shape);
          })
        }
      >
        Draw A Boundary
      </ActionStep>

      {writingLayerData && (
        <div className="flex space-x-2 justify-center items-center bg-white dark:invert">
          <span>Getting features from Overpass turbo</span>
          <div className="h-4 w-4 bg-black rounded-full animate-bounce [animation-delay:-0.3s]"></div>
          <div className="h-4 w-4 bg-black rounded-full animate-bounce [animation-delay:-0.15s]"></div>
          <div className="h-4 w-4 bg-black rounded-full animate-bounce"></div>
        </div>
      )}

      <ActionStep
        ix={"2a"}
        onClick={() =>
          rpc.invoke("updateLayerStyle", [
            TEMP_LAYER_NAME,
            {
              showLabels: true,
              displayKeys: ["name", "amenity"],
            },
          ])
        }
        disabled={overpassFeatures.length === 0}
      >
        Add labels to temp layer
      </ActionStep>

      <ActionStep
        ix={"2b"}
        onClick={() => rpc.invoke("setDrawTool", [{ tool: "MapSelector" }])}
        // disabled={overpassFeatures.length === 0}
      >
        Query the map
      </ActionStep>
      <button
        className="bg-white font-normal border text-gray-500 border-gray-500 px-2 hover:bg-gray-100"
        // disabled={overpassFeatures.length === 0}
        onClick={() =>
          rpc.invoke("enableBottomBarIframe", ["https://roland50.studio/", 500])
        }
      >
        Add iframe to bottom of map
      </button>

      <div className="text-center mt-auto  bg-blue-100 rounded-sm border border-blue-800">
        <a
          target="_blank"
          rel="noreferrer"
          href="https://wiki.openstreetmap.org/wiki/Overpass_API"
        >
          More about the Overpass API
        </a>
      </div>
    </div>
  );
}

export default App;
