import { Feature, Polygon } from "geojson";

export interface Tags {
  [k: string]: string;
  name: string;
  amenity: string;
}

export type Element = Feature & {
  tags?: Tags;
};

export type OverpassResponse = {
  version: string;
  elements: Element[];
};
const OVERPASS_API_URL = "https://overpass-api.de/api/interpreter";

export const fetchOverpass = (polygon: Feature<Polygon>) => {
  const poly = polygon.geometry.coordinates
    .flat()
    .reduce<number[]>((a, [lng, lat]) => (a.push(lat, lng), a), [])
    .join(" ");

  return fetch(OVERPASS_API_URL, {
    method: "POST",
    headers: {
      "Content-Type": "application/x-www-form-urlencoded",
    },
    body: `data=${encodeURIComponent(`
    [out:json]
    [timeout:90]
    ;
    (
      node["amenity"]["name"](poly:"${poly}");
      way["amenity"](poly:"${poly}");
      relation["amenity"](poly:"${poly}");
      <;
    );
    convert item ::=::,::geom=geom(),_osm_type=type();
    out geom;`)}`,
  })
    .then(r => r.json())
    .then(r => xformToFeatures(r));
};

const xformToFeatures = ({ elements }: OverpassResponse): Feature[] =>
  elements
    .filter(({ tags }) => !!tags?.name)
    .map(
      ({ tags, ...rest }) =>
        ({
          ...rest,
          type: "Feature",
          properties: tags,
        } as Feature)
    );
